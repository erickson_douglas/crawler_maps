# coding: utf8
from ast import literal_eval
from time import sleep
from csv import DictWriter
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from opt.lib_proxys import Proxys
from os import name as os_name
from urllib.parse import unquote_plus
from emoji import get_emoji_regexp


CLASSIFICATION = {'relevante': '2e4',
                  'recente': '3e2',
                  'maior': '3e3',
                  'menor': '3e4'}


class MapsSpider:
    def __init__(self):
        self._browser = None
        self._url = 'https://www.google.com.br'
        self._cookies = {}
        self._params = None
        self._total_comments = None
        self._click_time = None
        self._click_attempts = None
        self._all_comments = None
        self._proxy = Proxys()
        self._url_split = None
        self._result = 0

    def _import_csv(self):
        fieldnames = ['Name', 'Date', 'Rating', 'Star', 'Text']
        _name_file = open(f'csv/{self._params.get("name_export", "temp")}.csv', 'w', newline='')
        self._all_comments = DictWriter(_name_file, fieldnames)
        self._all_comments.writeheader()

    @staticmethod
    def __clean_emoji(text):
        return get_emoji_regexp().sub(r'', text)

    def __ativar_selenium(self):
        print("[INFO] ativar selenium")
        if self._params.get('text_search'):
            self._browser.get(f"{self._url}/maps")
            return self.__pesquisa_local()

        elif self._params.get('url_search'):
            self._url = "/".join(self._params["url_search"].split("/")[:3])
            self._browser.get(f'{self._url}/{"/".join(self._params["url_search"].split("/")[3:])}')
            return self.__entrar_comentarios()
        else:
            return None

    def __pesquisa_local(self, retries=0):
        print("[INFO] Pequisa local selenium")
        try:
            if retries > self._click_attempts:
                return None

            result = self._browser.find_element_by_xpath('//*[@id="searchboxinput"]')
            result.send_keys(self._params.get('text_search'))
            sleep(self._click_time)
            result.send_keys(Keys.RETURN)
            return self.__entrar_comentarios()

        except Exception as e:
            print("error:", e)
            return self.__pesquisa_local(retries=retries + 1)

    def __entrar_comentarios(self):
        print("[INFO] entrando comentario")
        try:
            sleep(self._click_time)
            if not self._total_comments:
                if self._params.get('limit_comment'):
                    self._total_comments = int(self._params['limit_comment'])
                else:
                    self._total_comments = int(self._browser.find_element_by_xpath(
                        "//button[@class='jqnFjrOWMVU__button gm2-caption']").text[:-11].replace('.', '')
                                               )
            self._browser.find_element_by_xpath("//button[@data-value='Classificar']").click()
            self._browser.find_elements_by_xpath("//li[@class='action-menu-entry']")[-1].click()
            return self.__detectar_url()
        except Exception as e:
            print("error:", e)
            return None

    def __detectar_url(self):
        print("[INFO] Detectar url")
        sleep(self._click_time)
        try:
            _logs = self._browser.execute_script("return window.performance.getEntries();")
            self._cookies = dict(map(lambda x: (x['name'], x['value']), self._browser.get_cookies()))
            for log in reversed(_logs):
                if log['name'].startswith(f'{self._url}/maps/preview/review/listentitiesreviews'):
                    _logs = log['name']
                    break

            if type(_logs) == str:
                return _logs
            return None
        except Exception as e:
            print("error:", e)
            return None

    def __execute_url(self, url):
        print("[INFO] Executar_url")
        self._url_split = url.split('!')
        self._url_split[7] = CLASSIFICATION.get(self._params.get("classification"), '2e4')
        for enu, num in enumerate(range(10, self._total_comments + 10, 10), 1):
            if enu % 41 == 0:
                print(f'URL: enu{enu} - num{num}')
                url = self.__entrar_comentarios()
                if url:
                    self._url_split = url.split("!")
                    self._url_split[7] = CLASSIFICATION.get(self._params.get("classification"), '2e4')

            if num > self._total_comments:
                self._url_split[5] = f'1i{self._total_comments - 1}'
            else:
                self._url_split[5] = f'1i{num}'
            print(f"total no momento: {self._result}\nproximo link para extrair: {'!'.join(self._url_split)}")
            self.__map_request("!".join(self._url_split))
        return {'status': 'success'}

    def __map_request(self, url, *, retries=0):
        if retries > self._click_attempts:
            return None
        try:
            res = self._proxy.random(url=url)
            res = literal_eval(res[5:].replace('null,', '').replace('\n', ''))[0] if res else None
            list(map(self.__map_json, res)) if res else None
        except ConnectionError:
            sleep(self._click_time)
            return self.__map_request(url=url, retries=retries + 1)

    def __map_json(self, lists) -> bool:
        result = {'Name': lists[0][1], 'Date': lists[1]}
        if type(lists[2]) is str:
            result.update({'Text': self.__clean_emoji(lists[2]), 'Star': lists[3]})
            if type(lists[6]) is str:
                if type(lists[8][0][1]) is int:
                    result['Rating'] = lists[8][0][1]
                else:
                    result['Rating'] = None
            else:
                # result.update({'owner': {'Date': lists[6][0], 'Text': lists[6][1]}})
                if type(lists[9][0][1]) is int:
                    result['Rating'] = lists[9][0][1]
                else:
                    result['Rating'] = None
        else:
            result.update({'Text': None, 'Star': lists[2]})
            if type(lists[5]) is str:
                if type(lists[7][0][1]) is int:
                    result['Rating'] = lists[7][0][1]
                else:
                    result['Rating'] = None
            else:
                # result.update({'owner': {'Date': lists[5][0], 'Text': lists[5][1]}})
                if type(lists[8][0][1]) is int:
                    result['Rating'] = lists[8][0][1]
                else:
                    result['Rating'] = None
        self._all_comments.writerow(result)
        self._result += 1
        return True

    def stalk(self):
        try:
            self._click_time = float(self._params.get("click_time", 5))
            self._click_attempts = float(self._params.get('click_attempts', 5))
            eval(f'self._import_{self._params.get("export", "csv")}()')
            option = Options()
            option.headless = True
            if os_name == "nt":
                _firefox = FirefoxBinary('C:\\Program Files\\Mozilla Firefox\\firefox.exe')
                self._browser = webdriver.Firefox(options=option, firefox_binary=_firefox,
                                                  executable_path=r'opt/geckodriver.exe')
            else:
                self._browser = webdriver.Firefox(options=option)
            url = self.__ativar_selenium()
            if url:
                self.__execute_url(url=url)
                return f"Salvo em csv/{self._params['name_export']}.{self._params['export']}, " \
                       f"total encontrado: {self._result}"
        except Exception as e:
            return "Tente novamente!"
        finally:
            self._browser.close()


if __name__ == '__main__':
    kwargs = {
        'text_search': '',
        'url_search': 'https://www.google.com.br/maps/place/Cabeleireiro+Masculino/@-23.5858944,-46.7184099,17z/data=!3m1!4b1!4m5!3m4!1s0x94ce56fb2b6299ed:0xbd8515da127b98f!8m2!3d-23.5858944!4d-46.7162212',
        'classification': 'relevante',
        'click_time': 5,
        'click_attempts': 3,
        'limit_comment': '',
        'name_export': 'roosevelt',
        'export': 'csv'
    }
    instance = MapsSpider()
    print("Bem vindo ao Discorevy Stars"
          "Antes de extrair os comentários do google maps, gostariamos de fazer uns perguntas\n")
    try:
        _option = int(input("Pretende extrair por[padrao=url]:  url=[0], arquivo_urls=[1], nome_local=[2] em test? ")
                      or 0)
        _option1 = str(input("Qual vai ser a ordem?[padrão='relevante']\n"
                             "opções[relevante, recente, maior, menor]? "))
        kwargs['classification'] = _option1 if _option1 in ['recente', 'maior', 'menor'] else "relevante"
        print("\nTodas as extração vai pasta 'csv'\n")
        if _option == 2:
            kwargs["text_search"] = str(input("Digite o nome do local: "))
            kwargs['name_export'] = kwargs['name_export']
            instance._params = kwargs
            print(instance.stalk())

        elif _option == 1:
            _option1 = str(input("Digite o nome do arquivo: "))
            with open(_option1, "r") as _urls:
                for _url in _urls:
                    kwargs['url_search'] = _url
                    kwargs['name_export'] = unquote_plus(_url).split("/")[5].replace("+", " ")
                    instance._params = kwargs
                    print(instance.stalk())
        else:
            kwargs["url_search"] = str(input("Digite o link: "))
            kwargs['name_export'] = unquote_plus(kwargs['url_search']).split("/")[5].replace("+", " ")
            instance._params = kwargs
            print(instance.stalk())
    except ValueError:
        print("Opção incorreto, tente novamente")
    sleep(60)
