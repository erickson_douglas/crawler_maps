# Crawler Google Maps - BETA
Extrair comentarios do Google Maps baseado no [scraper_google_map](https://gitlab.com/erickson_douglas/scraper_google_map)

## Preparando o ambiente
### Instale Firefox
### Linux
#### [geckdriver](https://medium.com/beelabacademy/baixando-e-configurando-o-geckodriver-no-ubuntu-dc2fe14d91c)
```sh
#abre o terminal e instale virtualenv e use o comando pra criar a pasta
$ git clone https://gitlab.com/erickson_douglas/crawler_maps.git && cd crawler_maps
$ virtualenv -p python3 venv && source venv/bin/activate
$ pip install -r requirements.txt
$ python3 scraper_otimized.py
```
### Windows
#### [Crawler Google Maps](https://gitlab.com/erickson_douglas/crawler_maps/-/raw/master/src/crawler-maps.zip)
Depois de extrair o zip
Click em scraper_otimized.py

###copie o link para extração
![img](https://gitlab.com/erickson_douglas/crawler_maps/-/raw/master/src/copie_link.png)
### Interativo
![img](https://gitlab.com/erickson_douglas/crawler_maps/-/raw/master/src/menu_test.png)
### Executando
![img](https://gitlab.com/erickson_douglas/crawler_maps/-/raw/master/src/menu_executing.png)
## Sugestões são bem vindo
